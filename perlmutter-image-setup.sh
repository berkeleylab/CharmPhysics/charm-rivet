#!/bin/bash

export ANALYSIS_PATH=`pwd`
cd ~/
source /global/cfs/cdirs/atlas/scripts/setupATLAS.sh
setupATLAS -3 -c centos7+batch --postsetup "cd ${ANALYSIS_PATH}; source perlmutter-setup.sh"
