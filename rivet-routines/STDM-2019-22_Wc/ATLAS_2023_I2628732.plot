BEGIN PLOT /ATLAS_2023_I2628732/d03-x01-y01
Title=D+ Transverse Momentum (W-D+ Channel)
XLabel=$p_\mathrm{T}(D^{+})$ [GeV]
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{+})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d04-x01-y01
Title=D+ Transverse Momentum (W+D- Channel)
XLabel=$p_\mathrm{T}(D^{-})$ [GeV]
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{-})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d05-x01-y01
Title=Absolute Lepton Pseudorapidity (W-D+ Channel)
XLabel=$|\eta(\ell)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d06-x01-y01
Title=Absolute Lepton Pseudorapidity (W+D- Channel)
XLabel=$|\eta(\ell)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d07-x01-y01
Title=D* Transverse Momentum (W-D*+ Channel)
XLabel=$p_\mathrm{T}(D^{*+})$ [GeV]
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{*+})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d08-x01-y01
Title=D* Transverse Momentum (W+D*- Channel)
XLabel=$p_\mathrm{T}(D^{*-})$ [GeV]
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{*-})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d09-x01-y01
Title=Absolute Lepton Pseudorapidity (W-D*+ Channel)
XLabel=$|\eta(\ell)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d10-x01-y01
Title=Absolute Lepton Pseudorapidity (W+D*- Channel)
XLabel=$|\eta(\ell)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d03-x01-y02
Title=D+ Transverse Momentum (W-D+ Channel)
XLabel=$p_\mathrm{T}(D^{+})$ [GeV]
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{+})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d04-x01-y02
Title=D+ Transverse Momentum (W+D- Channel)
XLabel=$p_\mathrm{T}(D^{-})$ [GeV]
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{+})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d05-x01-y02
Title=Absolute Lepton Pseudorapidity (W-D+ Channel)
XLabel=$|\eta(\ell)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d06-x01-y02
Title=Absolute Lepton Pseudorapidity (W+D- Channel)
XLabel=$|\eta(\ell)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d07-x01-y02
Title=D* Transverse Momentum (W-D*+ Channel)
XLabel=$p_\mathrm{T}(D^{*+})$ [GeV]
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{*+})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d08-x01-y02
Title=D* Transverse Momentum (W+D*- Channel)
XLabel=$p_\mathrm{T}(D^{*-})$ [GeV]
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(p_\mathrm{T}(D^{*-})$ [pb]
LogX=1
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d09-x01-y02
Title=Absolute Lepton Pseudorapidity (W-D*+ Channel)
XLabel=$|\eta(\ell)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/d10-x01-y02
Title=Absolute Lepton Pseudorapidity (W+D*- Channel)
XLabel=$|\eta(\ell)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(\ell)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_minus_Dplus_D_abs_eta
Title=Absolute D meson Pseudorapidity (W-D+ Channel)
XLabel=$|\eta(D)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_minus_Dplus_D_abs_eta_norm
Title=Absolute D meson Pseudorapidity (W-D+ Channel)
XLabel=$|\eta(D)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_minus_Dstar_D_abs_eta
Title=Absolute D meson Pseudorapidity (W-D*+ Channel)
XLabel=$|\eta(D)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_minus_Dstar_D_abs_eta_norm
Title=Absolute D meson Pseudorapidity (W-D*+ Channel)
XLabel=$|\eta(D)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_plus_Dplus_D_abs_eta
Title=Absolute D meson Pseudorapidity (W+D- Channel)
XLabel=$|\eta(D)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_plus_Dplus_D_abs_eta_norm
Title=Absolute D meson Pseudorapidity (W+D- Channel)
XLabel=$|\eta(D)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_plus_Dstar_D_abs_eta
Title=Absolute D meson Pseudorapidity (W+D*- Channel)
XLabel=$|\eta(D)|$
YLabel=$\int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2023_I2628732/lep_plus_Dstar_D_abs_eta_norm
Title=Absolute D meson Pseudorapidity (W+D*- Channel)
XLabel=$|\eta(D)|$
YLabel=$1 / \sigma \int \mathrm{d}\sigma / \mathrm{d}(|\eta(D)|)$ [pb]
LogY=0
END PLOT
