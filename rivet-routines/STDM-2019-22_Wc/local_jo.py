#!/bin/env python
theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = [
    '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.26345808._000720.pool.root.1',
    #'/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.26345863._002167.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.26345892._000895.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148211._000146.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148217._000416.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148250._000361.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148250._000539.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148282._000325.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148282._000410.pool.root.1',
    # '/global/cfs/cdirs/atlas/wcharm/EVNT/mc15_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351/EVNT.29148294._000459.pool.root.1',
]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()

rivet.Analyses += ['ATLAS_2023_I2628732']
rivet.RunName = ''
rivet.HistoFile = 'Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.700339.yoda.gz'
rivet.CrossSection = 2988.15971
# rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True # Set to False to include systematic variations via event weights
job += rivet
