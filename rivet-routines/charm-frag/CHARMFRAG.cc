// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/HeavyHadrons.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include <unordered_map>

namespace Rivet
{

  /// @brief Add a short analysis description here
  class CHARMFRAG : public Analysis
  {
  public:
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(CHARMFRAG);

    // Define relevant cuts
    double minElecPt = 30.0 * GeV;
    double minDPt = 5.0 * GeV;
    double maxDeta = 2.2;

    /// @name Analysis methods
    ///@{

    /// Book histograms and initialise projections before the run
    void init()
    {

      // Initialise and register projections

      // The basic final-state projection:
      // all final-state particles within
      // the given eta acceptance
      const FinalState fs(Cuts::abseta < 4.9);

      // The final-state particles declared above are clustered using FastJet with
      // the anti-kT algorithm and a jet-radius parameter 0.4
      // muons and neutrinos are excluded from the clustering
      FastJets jetfs(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jetfs, "jets");

      // FinalState of prompt photons and bare muons and electrons in the event
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);

      PromptFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON, false, false); // don't accept tau or mu decays

      PromptFinalState bare_lepsWithTauDK(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON, true, false); // accept tau but not mu decays

      // Dress the prompt bare leptons with prompt photons within dR < 0.1,
      // and apply some fiducial cuts on the dressed leptons
      Cut lepton_cuts = Cuts::abseta < 2.5 && Cuts::pT > minDPt;
      DressedLeptons dressed_leps(photons, bare_leps, 0.1, lepton_cuts);
      declare(dressed_leps, "leptons");

      // We will treat leptons from Taus as background but this
      // histogram will help us estimate the background from
      // tau decays as a function of pT
      DressedLeptons dressed_lepsWithTauDK(photons, bare_lepsWithTauDK, 0.1, lepton_cuts);
      declare(dressed_lepsWithTauDK, "leptonsWithTauDK");

      // Charmed Hadrons
      declare(UnstableParticles(), "UFS");

      // Book histograms
      //  cutflow histogram keeps info how many events or candidates pass each cut
      // This is not really a cutflow since the dilepton and semileptonic selections counted in the same histogram and these are
      // othogonal cuts
      // Defintion of bins for "cutflow":
      // 1=All events, 2=two btags, 3=+dilepton,
      // 4=weighted dilepton, 5=e+D, 6-e+D weighted,
      // 7=e+D*, 8=e+D+ weighted  9 and 10 unused for now
      book(_h["cutflow"], "cutflow", 10, 0, 10.0);
      book(_h["CharmVsSpecies"], "CharmVsSpecies", 8, 0, 8.0);
      book(_h["ptmu"], "ptmu", 400, 0.0, 400.0);
      book(_h["ptmuWithTauDK"], "ptmuWithTauDK", 400, 0.0, 400.0);
      book(_h["ptD"], "ptD", 400, 0.0, 400.0);
      book(_h["ptDstar"], "ptDstar", 400, 0.0, 400.0);
      book(_h["Nbjets"], "Nbjets", 10, -0.5, 9.5);
    }

    /// Perform the per-event analysis
    void analyze(const Event &event)
    {

      // cutflow bins incremented
      double cut = 0.5;
      _h["cutflow"]->fill(cut);
      cut++;
      // Retrieve dressed leptons, sorted by pT
      vector<DressedLepton> leptons =
          apply<DressedLeptons>(event, "leptons").dressedLeptons();

      vector<DressedLepton> leptonsWithTauDK =
          apply<DressedLeptons>(event, "leptonsWithTauDK").dressedLeptons();

      vector<DressedLepton> els, mus, musWithTauDK;
      float el_charge = 0;
      for (DressedLepton &lep : leptons)
      {
        if (lep.abspid() == 11 && lep.pt() > minElecPt)
        {
          els.push_back(lep);
          el_charge = lep.charge();
        }
        else if (lep.abspid() == 13 && fabs(lep.eta()) < maxDeta)
          mus.push_back(lep);
      }

      for (DressedLepton &lep : leptonsWithTauDK)
      {
        if (lep.abspid() == 13 && fabs(lep.eta()) < maxDeta)
          musWithTauDK.push_back(lep);
      }
      // retrieve jets, sorted by pT, with a minimum pT cut of 20 GeV
      Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 20 * GeV);
      // remove all jets within dR < 0.2 of a dressed lepton
      idiscardIfAnyDeltaRLess(jets, leptons, 0.2);

      int nbjet = 0;
      for (const Jet &jet : jets)
        if (jet.bTagged(Cuts::pT > minDPt && Cuts::abseta < 2.5))
          nbjet++;
      _h["Nbjets"]->fill(1.0 * nbjet);

      // Only analyze events with >=  2 b-jets
      if (nbjet < 2)
        return;

      _h["cutflow"]->fill(cut);
      cut++;
      // di-lepton channel
      if (els.size() == 1 && mus.size() == 1)
      {
        for (const Particle &lepmu : mus)
        {
          int nbj_OR = nBjet_afterOR(jets, lepmu);
          if (nbj_OR > 1)
          {
            float mu_charge = lepmu.charge();
            double weight = (mu_charge * el_charge < 0) ? +1.0 : -1.0;
            _h["ptmu"]->fill(lepmu.pT() / GeV, weight);
            _h["cutflow"]->fill(cut);
            cut++;
            _h["cutflow"]->fill(cut, weight);
            cut++;
          }
        }
      }
      // Use this to evaluate the tau background
      if (els.size() == 1 && musWithTauDK.size() == 1)
      {
        for (const Particle &lepmu : musWithTauDK)
        {
          int nbj_OR = nBjet_afterOR(jets, lepmu);
          if (nbj_OR > 1)
          {
            float mu_charge = lepmu.charge();
            double weight = (mu_charge * el_charge < 0) ? +1.0 : -1.0;
            _h["ptmuWithTauDK"]->fill(lepmu.pT() / GeV, weight);
          }
        }
      }

      // Get the charm hadrons
      // l+jets channel
      if (els.size() == 1 && mus.size() == 0)
      {
        const UnstableParticles &ufs = apply<UnstableFinalState>(event, "UFS");

        // Loop over particles to find charmed hadrons
        for (const Particle &p : ufs.particles())
        {
          const int id = p.abspid();
          double Dpt = p.pT() / GeV;
          double eta = fabs(p.eta());
          // Count b jets after overlap removal
          // Number of b-jets with Delta(R)>0.4 from the charmed hadron
          int nbj_OR = 0;
          if (_charm_hadron_names.count(id) && Dpt > minDPt && eta < maxDeta)
          {
            // Require b-jets after overlap removal
            nbj_OR = nBjet_afterOR(jets, p);

            if (nbj_OR > 1)
            {
              float charm_charge = p.pid() > 0 ? 1.0 : -1.0;
              double weight = (charm_charge * el_charge < 0) ? +1.0 : -1.0;
              _h["CharmVsSpecies"]->fill(_charm_species_map.at(id), weight);
              if (id == 411)
              {
                cut = 4.5;
                _h["ptD"]->fill(Dpt, weight);
                _h["cutflow"]->fill(cut);
                cut++;
                _h["cutflow"]->fill(cut, weight);
                cut++;
              }
              if (id == 413)
              {
                cut = 6.5;
                _h["ptDstar"]->fill(Dpt, weight);
                _h["cutflow"]->fill(cut);
                cut++;
                _h["cutflow"]->fill(cut, weight);
                cut++;
              }
            }
          }
        }
      }

      return;
    }

    /// Normalise histograms etc., after the run
    void finalize()
    {

      for (map<string, Histo1DPtr>::iterator hit = _h.begin(); hit != _h.end(); ++hit)
      {
        scale(hit->second, crossSectionPerEvent());
      }
    }

    int nBjet_afterOR(Jets jets, const ParticleBase &p)
    {
      int nbj_OR = 0;
      for (const Jet &jet : jets)
      {
        if (jet.bTagged(Cuts::pT > minDPt && Cuts::abseta < 2.5))
        {
          if (deltaR(jet, p) > 0.4)
            nbj_OR++;
        }
      }
      return nbj_OR;
    }

  private:
    // mapping between pdg id an charm hadron names
    const std::unordered_map<unsigned int, std::string> _charm_hadron_names =
        {
            {411, "Dplus"},
            {413, "Dstar"},
            {421, "Dzero"},
            {431, "Ds"},
            {4122, "LambdaC"},
            {4132, "XiCzero"},
            {4232, "XiCplus"},
            {4332, "OmegaC"},
        };

    // needed to fill the 'CharmVsSpecies' histogram
    const std::unordered_map<unsigned int, float> _charm_species_map =
        {
            {411, 1.5},
            {413, 0.5},
            {421, 2.5},
            {431, 3.5},
            {4122, 4.5},
            {4132, 5.5},
            {4232, 6.5},
            {4332, 7.5},
        };

    /// @name Histograms
    ///@{
    map<string, Histo1DPtr> _h;
    ///@}
  };

  DECLARE_RIVET_PLUGIN(CHARMFRAG);

}
