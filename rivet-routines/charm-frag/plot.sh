#!/bin/bash

export RIVET_ANALYSIS_PATH=$PWD
OUTFILE="CHARMFRAG.yoda.gz"

# plot
rivet-mkhtml --errs -o plots $OUTFILE:"Title=Sherpa2.2.11"
