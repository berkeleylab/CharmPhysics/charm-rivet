BEGIN PLOT /CHARMFRAG/CharmVsSpecies
Title=Charm Vs Species
XLabel=Species ($D^+$, $D^*$, $D^0$, $D_s$, $\lambda_C$, $\Xi_C^0$, $\Xi_C^+$, $\Omega_C$)
YLabel=Count
LogY=1
END PLOT

BEGIN PLOT /CHARMFRAG/cutflow
Title=Cutflow
XLabel=Cut (All, 2 btags, +Dilepton, W Dilepton, e+D, W -e+D ,e+D*,W e+D+)
END PLOT

BEGIN PLOT /CHARMFRAG/Nbjets
XLabel=Number of b-Jets
YLabel=Count
END PLOT

BEGIN PLOT /CHARMFRAG/ptD
XLabel=$p_T$
YLabel=Count
Rebin=15
LogY=0
END PLOT

BEGIN PLOT /CHARMFRAG/ptDstar
XLabel=$p_T$
YLabel=Count
Rebin=15
LogY=0
END PLOT

BEGIN PLOT /CHARMFRAG/ptmu
XLabel=$p_T$
YLabel=Count
Rebin=15
LogY=0
END PLOT

BEGIN PLOT /CHARMFRAG/ptmuWithTauDK
XLabel=$p_T$
YLabel=Count
Rebin=15
LogY=0
END PLOT