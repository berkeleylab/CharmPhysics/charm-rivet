#!/bin/env python
theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = [
    '/global/cfs/cdirs/atlas/shapiro/old13tev_wd_rivet/ttbar/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.EVNT.e6337/EVNT.32858505._000520.pool.root.1',
    # '/global/cfs/cdirs/atlas/shapiro/old13tev_wd_rivet/ttbar/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.EVNT.e6337/EVNT.32858505._000543.pool.root.1',
    # '/global/cfs/cdirs/atlas/shapiro/old13tev_wd_rivet/ttbar/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.EVNT.e6337/EVNT.32858505._001192.pool.root.1',
    # '/global/cfs/cdirs/atlas/shapiro/old13tev_wd_rivet/ttbar/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.EVNT.e6337/EVNT.32858505._001873.pool.root.1',
]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()

rivet.Analyses += ['CHARMFRAG']
rivet.RunName = ''
rivet.HistoFile = 'CHARMFRAG.yoda.gz'
rivet.CrossSection = 1
# rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True # Set to False to include systematic variations via event weights
job += rivet
